#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'




"""	"entete": {
			"scope": "python",
			"prefix": "entete",
			"body": [
				"#!/usr/bin/env python3",
				"# -*- coding:utf-8 -*-",
				"# version 1",
				"__version__ = '1'",""
			],
			"description": "entete python"
		},
"""


atom = open('snippets.cson').readlines()
vscode='res.json'
# print(atom)
debut = atom.index("'.source.python':\n")
indice = 0
with open(vscode, 'w') as f:
    for ligne in atom[debut+1:]:
        ligne=ligne.strip('\n')
        print(ligne)
        if ligne == '':
            continue
        if indice==0:
            r = ligne+" {\n"
            f.write(r.replace("'", "\""))
            f.write("""\"scope\": \"python\",\n""")
            indice+=1
        if 'prefix' in ligne:
            f.write(ligne.replace("'", "\"")+",\n")
        if 'body' in ligne:
            f.write('"body": ['+ '"'+ligne.split('"""')[1].replace('"', "\\\"").replace("'", "\\\"")+"\",")
            indice = 2
            continue
        if indice == 2:
            if '"""' in ligne:
                f.write('],\n')
                f.write('"description":"-"\n')
                f.write("},\n")
                indice = 0
            else:
                f.write('"' + ligne.replace('"', "\\\"").replace("'", "\\\"")+ '",\n')



